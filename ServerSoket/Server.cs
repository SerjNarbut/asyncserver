﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;


namespace ServerSoket
{
    class Server
    {
        public delegate void OnClienAcceptedHandler(Socket e);
        public event OnClienAcceptedHandler Accepted;

        public int Port { get; private set; }
        public string EndPoint { get; private set; }
        public bool IsListening { get; private set; }
        public HashSet<Client> Clients
        {
            get
            {
                return this.clients;
            }
        }

        private Socket serverSocket;
        private HashSet<Client> clients;

        public Server(string name, int port)
        {
            this.Port = port;
            this.EndPoint = name;
            this.IsListening = false;
            this.clients = new HashSet<Client>();
        }

        public void Start()
        {
            if (!this.IsListening)
            {
                this.serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                this.serverSocket.Bind(new IPEndPoint(IPAddress.Parse(this.EndPoint),this.Port));
                this.serverSocket.Listen(0);
                this.serverSocket.BeginAccept(this.acyncAcceptCallback, null);
                this.IsListening = true;
                this.clients = new HashSet<Client>();
            }
            return;
        }

        public void Stop()
        {
            if (this.IsListening)
            {
                this.serverSocket.Close();
                this.serverSocket.Dispose();
                this.IsListening = false;
                this.clients.Clear();
                this.clients = null;
            }
            return;
        }


        private void acyncAcceptCallback(IAsyncResult ar)
        {
            try
            {
                Socket acc = this.serverSocket.EndAccept(ar);
                if (this.Accepted != null)
                    this.Accepted(acc);
                this.serverSocket.BeginAccept(this.acyncAcceptCallback, null);
            }
            catch (ObjectDisposedException)
            {
                return;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }


        public static void Send(Byte[] data, Client cls)
        {
            cls.Socket.BeginSend(data, 0, data.Length, SocketFlags.None, Server.asyncSend, cls.Socket);
        }

        private static void asyncSend(IAsyncResult ar)
        {
            try
            {
                Socket cls = (Socket)ar.AsyncState;
                int sd = cls.EndSend(ar);
                Console.WriteLine("Send by server:\t{0} bytes", sd);
            }
            catch (ObjectDisposedException) { return; }
            catch (SocketException) { return; }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
