﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace ServerSoket
{
    class Program
    {
        static Server server;
        static void Main(string[] args)
        {
            server = new Server("127.0.0.1", 911);
            server.Accepted += server_Accepted;
            server.Start();
            while (true)
            {
                String message = Console.ReadLine();
                if (message != String.Empty)
                {
                    if (message == "exit")
                    {
                        break;
                    }
                    foreach (Client c in server.Clients)
                    {
                        Server.Send(Encoding.Default.GetBytes(message), c);
                    }
                }
            }
            Console.ReadLine();
            server.Stop();
        }

        static void server_Accepted(System.Net.Sockets.Socket e)
        {
            Client cls = new Client(e);
            server.Clients.Add(cls);
            Console.WriteLine("New connection:\t{0}\t{1}", cls.EndPoint, DateTime.Now);
            cls.Recived += cls_Recived;
            cls.Disconnect += cls_Disconnect;
        }

        static void cls_Disconnect(Client client)
        {
            Console.WriteLine("Dissconnected:\t{0}\t{1}", client.EndPoint, DateTime.Now);
            client.Close();
        }

        static void cls_Recived(Client client, byte[] data)
        {
            String b = Encoding.Default.GetString(data);
            Console.WriteLine("Message from {0}:\t{1}",client.EndPoint,b);
            Server.Send(Encoding.Default.GetBytes("echo from server: " + b), client);
        }
    }
}
