﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;


namespace ServerSoket
{
    class Client
    {
        private Socket socket;
        public delegate void RecivedHandler(Client client, byte[] data);
        public event RecivedHandler Recived;

        public delegate void DisconnectHandler(Client client);
        public event DisconnectHandler Disconnect;

        public Socket Socket { get { return this.socket; } }

        public IPEndPoint EndPoint;

        public Client(Socket s)
        {
            this.socket = s;
            this.EndPoint = (IPEndPoint)s.RemoteEndPoint;
            this.socket.BeginReceive(new byte[0] { }, 0, 0, 0, this.asyncRecive, null);
        }

        public void Close()
        {
            this.socket.Close();
            this.socket.Dispose();
        }


        private void asyncRecive(IAsyncResult ar)
        {
            try
            {
                this.socket.EndReceive(ar);

                byte[] buffer = new byte[1024];
                int recSize = this.socket.Receive(buffer, buffer.Length, 0);
                if (recSize == 0)
                {
                    if (Disconnect != null)
                        this.Disconnect(this);
                    return;
                }
                if (recSize < buffer.Length)
                {
                    Array.Resize<byte>(ref buffer, recSize);
                }
                if (this.Recived != null)
                    this.Recived(this, buffer);

                this.socket.BeginReceive(new byte[0] { }, 0, 0, 0, this.asyncRecive, null);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                if (Disconnect != null)
                    this.Disconnect(this);
            }
        }


    }
}
