﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace ClientSocket
{
    class Program
    {
        static void Main(string[] args)
        {
            string ip;
            if (args.Length < 1)
                ip = "127.0.0.1";
            else
                ip = args[0];
            Client cls = new Client();
            cls.Recived += cls_Recived;
            cls.Disconnect += cls_Disconnect;
            cls.Connect(ip, 911);
            while (true)
            {
                string data = Console.ReadLine();
                if (data == "exit")
                {
                    cls.Close();
                    break;
                }
                cls.Send(Encoding.Default.GetBytes(data));
            }

        }

        static void cls_Disconnect(Client client)
        {
            client.Close();
        }

        static void cls_Recived(Client client, byte[] data)
        {
            Console.WriteLine("Message from server :\t{0}",Encoding.Default.GetString(data));
        }
    }
}
