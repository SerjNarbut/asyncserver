﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace ClientSocket
{
    class Client
    {
        public delegate void ConnetedHandler(Client sender, bool connected);
        public event ConnetedHandler OnConnect;

        public delegate void SendHandler(Client client);
        public event SendHandler OnSend;

        public delegate void RecivedHandler(Client client, byte[] data);
        public event RecivedHandler Recived;

        public delegate void DisconnectHandler(Client client);
        public event DisconnectHandler Disconnect;

        private Socket s;

        public bool Connected
        {
            get
            {
                if (this.s != null)
                    return s.Connected;
                return false;
            }
        }

        public Client()
        {
            this.s = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        }

        public void Connect(string ip, int port)
        {
            if (this.s == null)
                this.s = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            this.s.BeginConnect(ip, port, asycConnected, null);
        }

        public void Close()
        {
            if (this.s != null)
            {
                this.s.Disconnect(false);
                this.s.Close();
            }
            this.s = null;
        }

        public void Send(byte[] data)
        {
            if (this.Connected)
            {
                this.s.BeginSend(data, 0, data.Length, 0, null, null);
            }
        }

        private void asyncSend(IAsyncResult iar)
        {
            try
            {
                this.s.EndSend(iar);
                if (this.OnSend != null)
                    OnSend(this);
            }
            catch { }
        }

        private void asycConnected(IAsyncResult iar)
        {
            try
            {
                this.s.EndConnect(iar);
               if(this.s.Connected)
                    this.s.BeginReceive(new byte[0] { }, 0, 0, 0, this.asyncRecive, null);
                if (this.OnConnect != null)
                {
                    OnConnect(this, this.Connected);
                }
            }
            catch { }
        }

        private void asyncRecive(IAsyncResult ar)
        {
            try
            {
                this.s.EndReceive(ar);

                byte[] buffer = new byte[1024];
                int recSize = this.s.Receive(buffer, buffer.Length, 0);
                if (recSize == 0)
                {
                    if (this.Disconnect != null)
                        Disconnect(this);
                    return;
                }
                if (recSize < buffer.Length)
                {
                    Array.Resize<byte>(ref buffer, recSize);
                }
                if (this.Recived != null)
                    this.Recived(this, buffer);

                this.s.BeginReceive(new byte[0] { }, 0, 0, 0, this.asyncRecive, null);
            }
            catch (SocketException e)
            {
                switch (e.SocketErrorCode)
                {
                    case SocketError.ConnectionAborted:
                    case SocketError.ConnectionReset:
                        if (this.Disconnect != null)
                        {
                            Disconnect(this);
                            return;
                        }
                        break;
                }
            }
            catch (ObjectDisposedException)
            {
                return;
            }
            catch (NullReferenceException)
            {
                return;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.Message);
            }
        }

    }
}
